from django.core.mail import send_mail
from django.db.models.aggregates import Count
from django.http.response import HttpResponseRedirect, HttpResponse, HttpResponseNotAllowed
from django.shortcuts import get_object_or_404, render
from django.urls.base import reverse
from django.views.generic import TemplateView, DetailView, ListView
from django.views.generic.edit import CreateView

from jedicrm.main.models import Jedi, CandidateTest, Candidate, \
    CandidateTestQuestion, CandidateTestAnswer, Padawan


class IndexView(TemplateView):
    template_name = 'main/index.html'


class AllJediListView(ListView):
    template_name = 'main/jedi_list_all.html'
    queryset = Jedi.objects.annotate(padawans_num=Count('padawans'))


class JediTeachersView(ListView):
    template_name = 'main/jedi_list_teachers.html'
    queryset = Jedi.objects.annotate(
        padawans_num=Count('padawans')).filter(padawans_num__gt=1)


class JediDetails(DetailView):
    template_name = 'main/jedi_details.html'
    model = Jedi

    def get_context_data(self, **kwargs):
        context = super(JediDetails, self).get_context_data(**kwargs)
        jedi = context['jedi']

        context['success_candidates'] = Candidate.objects.filter(
            planet=jedi.planet, test_complete=True)

        return context


class CandidateDetailView(DetailView):
    template_name = 'main/candidate_detail.html'
    model = Candidate

    def get_context_data(self, **kwargs):
        context = super(CandidateDetailView, self).get_context_data(**kwargs)

        context['answers'] = CandidateTestAnswer.objects.filter(candidate=kwargs['object'])

        return context


class CandidateCreateView(CreateView):
    model = Candidate
    template_name = 'main/candidate_form.html'
    fields = '__all__'

    def get_success_url(self):

        # Из тз не понятно как выбирать тест для кандидата, поэтому берем любой (первый)
        test_id = CandidateTest.objects.first().pk

        return reverse('candidate_test_create', args=(self.object.id, test_id))


def candidate_test_create(request, pk, test_id):
    candidate = get_object_or_404(Candidate, pk=pk)
    questions = CandidateTestQuestion.objects.filter(test_id=test_id)

    if request.method == 'GET':
        return HttpResponse(render(request, 'main/candidate_test.html', {
            'candidate': candidate, 'questions': questions, 'test_id': test_id
        }))

    else:

        answers = [
            int(a)
            for a in request.POST
            if a.isdigit() and request.POST[a] == 'on'
        ]

        for question in questions:
            CandidateTestAnswer(
                question=question,
                candidate=candidate,
                value=True if question.id in answers else False
            ).save()

        candidate.test_complete = not bool(
            set(answers) ^ set([q.pk for q in questions.filter(answer=True)]))
        candidate.save()

        return HttpResponseRedirect(reverse('index'))


def adopt_new_padawan(request, jedi_id, candidate_id):

    if request.method == 'GET':
        return HttpResponseNotAllowed(('POST', ))

    else:

        candidate = get_object_or_404(Candidate, pk=candidate_id)
        jedi = get_object_or_404(Jedi, pk=jedi_id)

        if jedi.padawans.count() >= 3:
            return HttpResponse(render(request, 'main/error_page.html', context={
                'error_detail': 'You should not train more than 3 padawans at a time'}))

        Padawan(
            name=candidate.name,
            teacher=jedi
        ).save()

        send_mail(
            'Jedi academy: Responding to your request',
            'Your request is considered positive, and {} is now a padawan. '
            'Teacher {} will be here soon, prepare your towel.'.format(candidate.name, jedi.name),
            'jediacademy@galaxymail.re',
            [candidate.email],
        )

        candidate.delete()

        return HttpResponseRedirect(reverse('jedi_details', args=(jedi.id, )))

