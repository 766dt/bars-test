# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-04-07 18:02
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Candidate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=127, verbose_name='Name')),
                ('email', models.EmailField(max_length=254)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CandidateTest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('jedi_order_uuid', models.UUIDField(default=uuid.uuid4, editable=False)),
                ('is_correct', models.BooleanField()),
                ('candidate', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Candidate')),
            ],
        ),
        migrations.CreateModel(
            name='CandidateTestQuestion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField(verbose_name='Question text')),
                ('answer', models.BooleanField(editable=False)),
                ('correct_answer', models.BooleanField()),
                ('test', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.CandidateTest')),
            ],
        ),
        migrations.CreateModel(
            name='Jedi',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=127, verbose_name='Name')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Padawan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=127, verbose_name='Name')),
                ('teacher', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='padawans', to='main.Jedi')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Planet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=127, verbose_name='Planet name')),
            ],
        ),
        migrations.AddField(
            model_name='jedi',
            name='planet',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Planet'),
        ),
        migrations.AddField(
            model_name='candidate',
            name='planet',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Planet'),
        ),
        migrations.AddField(
            model_name='candidate',
            name='teacher',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Jedi'),
        ),
    ]
