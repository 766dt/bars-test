from django.contrib import admin

from jedicrm.main.models import Jedi, Planet, CandidateTest, CandidateTestQuestion


class JediAdmin(admin.ModelAdmin):
    list_display = ('name', 'get_planet_name')

    def get_planet_name(self, obj):
        return obj.planet.name

    get_planet_name.short_description = 'Planet'


class PlanetAdmin(admin.ModelAdmin):
    list_display = ('name', )

    def __str__(self):
        return 'Planet ' + self.name


class TestQuestionInline(admin.TabularInline):
    model = CandidateTestQuestion


class CandidateTestAdmin(admin.ModelAdmin):
    list_display = ('jedi_order_uuid', )
    inlines = (TestQuestionInline, )


admin.site.register(Jedi, JediAdmin)
admin.site.register(Planet, PlanetAdmin)
admin.site.register(CandidateTest, CandidateTestAdmin)
