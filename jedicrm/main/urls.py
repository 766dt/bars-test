# -*- coding: utf-8 -*-
from django.conf.urls import url

from jedicrm.main.views import *

urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'jedi/$', AllJediListView.as_view()),
    url(r'jedi/all/$', AllJediListView.as_view()),
    url(r'jedi/teachers/$', JediTeachersView.as_view()),
    url(r'jedi/(?P<pk>\d+)/$', JediDetails.as_view(), name='jedi_details'),
    url(r'jedi/(?P<jedi_id>\d+)/adopt/(?P<candidate_id>\d+)/$', adopt_new_padawan, name='adopt_new_padawan'),
    url(r'candidate/$', CandidateCreateView.as_view()),
    # url(r'candidate/(?P<pk>\d+)/test/$', CandidateTestCreateView.as_view(), name='candidate_test_create'),
    url(r'candidate/(?P<pk>\d+)/test/(?P<test_id>\d+)/$', candidate_test_create, name='candidate_test_create'),
    url(r'candidate/(?P<pk>\d+)/$', CandidateDetailView.as_view(), name='candidate_details'),
]
