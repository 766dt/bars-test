import uuid
from django.db import models


class Planet(models.Model):
    name = models.CharField(max_length=127, verbose_name='Planet name')

    def __str__(self):
        return self.name


class Person(models.Model):

    name = models.CharField(max_length=127, verbose_name='Name')

    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class Jedi(Person):
    planet = models.ForeignKey(Planet)

    class Meta:
        verbose_name_plural = 'Jedi'


class Padawan(Person):
    teacher = models.ForeignKey(Jedi, related_name='padawans')


class Candidate(Person):
    name = models.CharField(max_length=127)
    planet = models.ForeignKey(Planet)
    age = models.IntegerField(default=0)
    email = models.EmailField()

    test_complete = models.NullBooleanField(editable=False, blank=True)


class CandidateTest(models.Model):
    jedi_order_uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=False)


class CandidateTestQuestion(models.Model):
    test = models.ForeignKey(CandidateTest)

    question = models.TextField(verbose_name='Question text')
    answer = models.BooleanField(verbose_name='Is it true')


class CandidateTestAnswer(models.Model):

    question = models.ForeignKey(CandidateTestQuestion)
    candidate = models.ForeignKey(Candidate)
    value = models.BooleanField()




